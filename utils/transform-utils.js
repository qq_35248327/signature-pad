function base64ToBlob(dataURL) {
  let arr = dataURL.split(',')
  let mimeType = arr[0].match(/:(.*?);/)[1] || 'image/png'
  let encodeStr = atob(arr[1])
  let n = encodeStr.length
  let u8Arr = new Uint8Array(n)
  while (n--) {
    u8Arr[n] = encodeStr.charCodeAt(n)
  }
  return new Blob([u8Arr], { type: mimeType })
}

function blobToFile(blob, fileName) {
  // MIME Type
  const type = blob.type || 'text/plain'
  return new File([blob], fileName, { type })
}